﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form3X3
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCalcular3x3 = New System.Windows.Forms.Button()
        Me.tbx11 = New System.Windows.Forms.TextBox()
        Me.tbx12 = New System.Windows.Forms.TextBox()
        Me.tbx13 = New System.Windows.Forms.TextBox()
        Me.tbx23 = New System.Windows.Forms.TextBox()
        Me.tbx22 = New System.Windows.Forms.TextBox()
        Me.tbx21 = New System.Windows.Forms.TextBox()
        Me.tbx33 = New System.Windows.Forms.TextBox()
        Me.tbx32 = New System.Windows.Forms.TextBox()
        Me.tbx31 = New System.Windows.Forms.TextBox()
        Me.tbxResM3X3 = New System.Windows.Forms.TextBox()
        Me.lblResultado = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnCalcular3x3
        '
        Me.btnCalcular3x3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalcular3x3.Location = New System.Drawing.Point(248, 154)
        Me.btnCalcular3x3.Name = "btnCalcular3x3"
        Me.btnCalcular3x3.Size = New System.Drawing.Size(100, 95)
        Me.btnCalcular3x3.TabIndex = 0
        Me.btnCalcular3x3.Text = "Calcular"
        Me.btnCalcular3x3.UseVisualStyleBackColor = True
        '
        'tbx11
        '
        Me.tbx11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx11.Location = New System.Drawing.Point(12, 12)
        Me.tbx11.Name = "tbx11"
        Me.tbx11.Size = New System.Drawing.Size(88, 26)
        Me.tbx11.TabIndex = 1
        '
        'tbx12
        '
        Me.tbx12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx12.Location = New System.Drawing.Point(129, 12)
        Me.tbx12.Name = "tbx12"
        Me.tbx12.Size = New System.Drawing.Size(88, 26)
        Me.tbx12.TabIndex = 2
        '
        'tbx13
        '
        Me.tbx13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx13.Location = New System.Drawing.Point(248, 12)
        Me.tbx13.Name = "tbx13"
        Me.tbx13.Size = New System.Drawing.Size(88, 26)
        Me.tbx13.TabIndex = 3
        '
        'tbx23
        '
        Me.tbx23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx23.Location = New System.Drawing.Point(248, 56)
        Me.tbx23.Name = "tbx23"
        Me.tbx23.Size = New System.Drawing.Size(88, 26)
        Me.tbx23.TabIndex = 6
        '
        'tbx22
        '
        Me.tbx22.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx22.Location = New System.Drawing.Point(129, 56)
        Me.tbx22.Name = "tbx22"
        Me.tbx22.Size = New System.Drawing.Size(88, 26)
        Me.tbx22.TabIndex = 5
        '
        'tbx21
        '
        Me.tbx21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx21.Location = New System.Drawing.Point(12, 56)
        Me.tbx21.Name = "tbx21"
        Me.tbx21.Size = New System.Drawing.Size(88, 26)
        Me.tbx21.TabIndex = 4
        '
        'tbx33
        '
        Me.tbx33.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx33.Location = New System.Drawing.Point(248, 99)
        Me.tbx33.Name = "tbx33"
        Me.tbx33.Size = New System.Drawing.Size(88, 26)
        Me.tbx33.TabIndex = 9
        '
        'tbx32
        '
        Me.tbx32.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx32.Location = New System.Drawing.Point(129, 99)
        Me.tbx32.Name = "tbx32"
        Me.tbx32.Size = New System.Drawing.Size(88, 26)
        Me.tbx32.TabIndex = 8
        '
        'tbx31
        '
        Me.tbx31.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx31.Location = New System.Drawing.Point(12, 99)
        Me.tbx31.Name = "tbx31"
        Me.tbx31.Size = New System.Drawing.Size(88, 26)
        Me.tbx31.TabIndex = 7
        '
        'tbxResM3X3
        '
        Me.tbxResM3X3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbxResM3X3.Location = New System.Drawing.Point(101, 158)
        Me.tbxResM3X3.Name = "tbxResM3X3"
        Me.tbxResM3X3.Size = New System.Drawing.Size(100, 26)
        Me.tbxResM3X3.TabIndex = 10
        '
        'lblResultado
        '
        Me.lblResultado.AutoSize = True
        Me.lblResultado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultado.Location = New System.Drawing.Point(13, 158)
        Me.lblResultado.Name = "lblResultado"
        Me.lblResultado.Size = New System.Drawing.Size(82, 20)
        Me.lblResultado.TabIndex = 11
        Me.lblResultado.Text = "Resultado"
        '
        'form3X3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(360, 261)
        Me.Controls.Add(Me.lblResultado)
        Me.Controls.Add(Me.tbxResM3X3)
        Me.Controls.Add(Me.tbx33)
        Me.Controls.Add(Me.tbx32)
        Me.Controls.Add(Me.tbx31)
        Me.Controls.Add(Me.tbx23)
        Me.Controls.Add(Me.tbx22)
        Me.Controls.Add(Me.tbx21)
        Me.Controls.Add(Me.tbx13)
        Me.Controls.Add(Me.tbx12)
        Me.Controls.Add(Me.tbx11)
        Me.Controls.Add(Me.btnCalcular3x3)
        Me.Name = "form3X3"
        Me.Text = "Determinante de 3x3"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCalcular3x3 As Button
    Friend WithEvents tbx11 As TextBox
    Friend WithEvents tbx12 As TextBox
    Friend WithEvents tbx13 As TextBox
    Friend WithEvents tbx23 As TextBox
    Friend WithEvents tbx22 As TextBox
    Friend WithEvents tbx21 As TextBox
    Friend WithEvents tbx33 As TextBox
    Friend WithEvents tbx32 As TextBox
    Friend WithEvents tbx31 As TextBox
    Friend WithEvents tbxResM3X3 As TextBox
    Friend WithEvents lblResultado As Label
End Class
