﻿Public Class form4x4
    Private Function calcularMatris4x4() As Decimal
        Dim m4x4(16) As Decimal
        Dim matrisTextBox16() As TextBox = {tbx11, tbx12, tbx13, tbx14, tbx21, tbx22, tbx23, tbx24, tbx31, tbx32, tbx33, tbx34, tbx41, tbx42, tbx43, tbx44}
        For i = 0 To 15
            m4x4(i) = matrisTextBox16(i).Text
        Next
        'primera matris
        Dim dPrincipal1 As Decimal
        Dim dSecundaria1 As Decimal
        Dim dDeterminante1 As Decimal
        'segunda matris
        Dim dPrincipal2 As Decimal
        Dim dSecundaria2 As Decimal
        Dim dDeterminante2 As Decimal
        'tercera matriz
        Dim dPrincipal3 As Decimal
        Dim dSecundaria3 As Decimal
        Dim dDeterminante3 As Decimal
        'tercera matris
        Dim dPrincipal4 As Decimal
        Dim dSecundaria4 As Decimal
        Dim dDeterminante4 As Decimal
        'Resolviendo primera matris
        dPrincipal1 = (m4x4(5) * m4x4(10) * m4x4(15)) + (m4x4(6) * m4x4(11) * m4x4(13)) + (m4x4(7) * m4x4(9) * m4x4(14))
        dSecundaria1 = (m4x4(7) * m4x4(10) * m4x4(13)) + (m4x4(5) * m4x4(11) * m4x4(14)) + (m4x4(6) * m4x4(9) * m4x4(15))
        dDeterminante1 = m4x4(0) * (dPrincipal1 - dSecundaria1)
        'resolviendo la segunda matris
        dPrincipal2 = (m4x4(4) * m4x4(10) * m4x4(15)) + (m4x4(6) * m4x4(11) * m4x4(12)) + (m4x4(7) * m4x4(8) * m4x4(14))
        dSecundaria2 = (m4x4(7) * m4x4(10) * m4x4(12)) + (m4x4(4) * m4x4(11) * m4x4(14)) + (m4x4(6) * m4x4(8) * m4x4(15))
        dDeterminante2 = -(m4x4(1)) * (dPrincipal2 - dSecundaria2)
        'resolviendo la tercera matris
        dPrincipal3 = (m4x4(4) * m4x4(9) * m4x4(15)) + (m4x4(5) * m4x4(11) * m4x4(12)) + (m4x4(7) * m4x4(8) * m4x4(13))
        dSecundaria3 = (m4x4(7) * m4x4(9) * m4x4(12)) + (m4x4(4) * m4x4(11) * m4x4(13)) + (m4x4(5) * m4x4(8) * m4x4(15))
        dDeterminante3 = m4x4(2) * (dPrincipal3 - dSecundaria3)
        'resolviendo la cuarta matriz
        dPrincipal4 = (m4x4(4) * m4x4(9) * m4x4(14)) + (m4x4(6) * m4x4(8) * m4x4(13)) + (m4x4(5) * m4x4(10) * m4x4(12))
        dSecundaria4 = (m4x4(6) * m4x4(9) * m4x4(12)) + (m4x4(4) * m4x4(10) * m4x4(13)) + (m4x4(5) * m4x4(10) * m4x4(12))
        dDeterminante4 = -(m4x4(3)) * (dPrincipal4 - dSecundaria4)
        Return dDeterminante1 + dDeterminante2 + dDeterminante3 + dDeterminante4
    End Function

    Private Sub btnCalcular4x4_Click(sender As Object, e As EventArgs) Handles btnCalcular4x4.Click
        tbxResM4X4.Text = calcularMatris4x4()
    End Sub
End Class