﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnDet3x3 = New System.Windows.Forms.Button()
        Me.btnDet4x4 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnDet3x3
        '
        Me.btnDet3x3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDet3x3.Location = New System.Drawing.Point(51, 36)
        Me.btnDet3x3.Name = "btnDet3x3"
        Me.btnDet3x3.Size = New System.Drawing.Size(161, 39)
        Me.btnDet3x3.TabIndex = 0
        Me.btnDet3x3.Text = "Determinante 3x3"
        Me.btnDet3x3.UseVisualStyleBackColor = True
        '
        'btnDet4x4
        '
        Me.btnDet4x4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDet4x4.Location = New System.Drawing.Point(51, 103)
        Me.btnDet4x4.Name = "btnDet4x4"
        Me.btnDet4x4.Size = New System.Drawing.Size(161, 39)
        Me.btnDet4x4.TabIndex = 1
        Me.btnDet4x4.Text = "Determinante 4x4"
        Me.btnDet4x4.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(283, 202)
        Me.Controls.Add(Me.btnDet4x4)
        Me.Controls.Add(Me.btnDet3x3)
        Me.Name = "Form1"
        Me.Text = "Determinantes opciones"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnDet3x3 As Button
    Friend WithEvents btnDet4x4 As Button
End Class
