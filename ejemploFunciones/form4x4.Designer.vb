﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form4x4
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCalcular4x4 = New System.Windows.Forms.Button()
        Me.tbx11 = New System.Windows.Forms.TextBox()
        Me.tbx12 = New System.Windows.Forms.TextBox()
        Me.tbx13 = New System.Windows.Forms.TextBox()
        Me.tbx14 = New System.Windows.Forms.TextBox()
        Me.tbx24 = New System.Windows.Forms.TextBox()
        Me.tbx23 = New System.Windows.Forms.TextBox()
        Me.tbx22 = New System.Windows.Forms.TextBox()
        Me.tbx21 = New System.Windows.Forms.TextBox()
        Me.tbx34 = New System.Windows.Forms.TextBox()
        Me.tbx33 = New System.Windows.Forms.TextBox()
        Me.tbx32 = New System.Windows.Forms.TextBox()
        Me.tbx31 = New System.Windows.Forms.TextBox()
        Me.tbx44 = New System.Windows.Forms.TextBox()
        Me.tbx43 = New System.Windows.Forms.TextBox()
        Me.tbx42 = New System.Windows.Forms.TextBox()
        Me.tbx41 = New System.Windows.Forms.TextBox()
        Me.tbxResM4X4 = New System.Windows.Forms.TextBox()
        Me.lblResultado = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnCalcular4x4
        '
        Me.btnCalcular4x4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalcular4x4.Location = New System.Drawing.Point(335, 227)
        Me.btnCalcular4x4.Name = "btnCalcular4x4"
        Me.btnCalcular4x4.Size = New System.Drawing.Size(115, 84)
        Me.btnCalcular4x4.TabIndex = 0
        Me.btnCalcular4x4.Text = "Calcular"
        Me.btnCalcular4x4.UseVisualStyleBackColor = True
        '
        'tbx11
        '
        Me.tbx11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx11.Location = New System.Drawing.Point(40, 25)
        Me.tbx11.Name = "tbx11"
        Me.tbx11.Size = New System.Drawing.Size(78, 26)
        Me.tbx11.TabIndex = 1
        '
        'tbx12
        '
        Me.tbx12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx12.Location = New System.Drawing.Point(147, 25)
        Me.tbx12.Name = "tbx12"
        Me.tbx12.Size = New System.Drawing.Size(78, 26)
        Me.tbx12.TabIndex = 2
        '
        'tbx13
        '
        Me.tbx13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx13.Location = New System.Drawing.Point(249, 25)
        Me.tbx13.Name = "tbx13"
        Me.tbx13.Size = New System.Drawing.Size(78, 26)
        Me.tbx13.TabIndex = 3
        '
        'tbx14
        '
        Me.tbx14.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx14.Location = New System.Drawing.Point(356, 25)
        Me.tbx14.Name = "tbx14"
        Me.tbx14.Size = New System.Drawing.Size(78, 26)
        Me.tbx14.TabIndex = 4
        '
        'tbx24
        '
        Me.tbx24.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx24.Location = New System.Drawing.Point(356, 66)
        Me.tbx24.Name = "tbx24"
        Me.tbx24.Size = New System.Drawing.Size(78, 26)
        Me.tbx24.TabIndex = 8
        '
        'tbx23
        '
        Me.tbx23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx23.Location = New System.Drawing.Point(249, 66)
        Me.tbx23.Name = "tbx23"
        Me.tbx23.Size = New System.Drawing.Size(78, 26)
        Me.tbx23.TabIndex = 7
        '
        'tbx22
        '
        Me.tbx22.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx22.Location = New System.Drawing.Point(147, 66)
        Me.tbx22.Name = "tbx22"
        Me.tbx22.Size = New System.Drawing.Size(78, 26)
        Me.tbx22.TabIndex = 6
        '
        'tbx21
        '
        Me.tbx21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx21.Location = New System.Drawing.Point(40, 66)
        Me.tbx21.Name = "tbx21"
        Me.tbx21.Size = New System.Drawing.Size(78, 26)
        Me.tbx21.TabIndex = 5
        '
        'tbx34
        '
        Me.tbx34.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx34.Location = New System.Drawing.Point(356, 108)
        Me.tbx34.Name = "tbx34"
        Me.tbx34.Size = New System.Drawing.Size(78, 26)
        Me.tbx34.TabIndex = 12
        '
        'tbx33
        '
        Me.tbx33.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx33.Location = New System.Drawing.Point(249, 108)
        Me.tbx33.Name = "tbx33"
        Me.tbx33.Size = New System.Drawing.Size(78, 26)
        Me.tbx33.TabIndex = 11
        '
        'tbx32
        '
        Me.tbx32.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx32.Location = New System.Drawing.Point(147, 108)
        Me.tbx32.Name = "tbx32"
        Me.tbx32.Size = New System.Drawing.Size(78, 26)
        Me.tbx32.TabIndex = 10
        '
        'tbx31
        '
        Me.tbx31.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx31.Location = New System.Drawing.Point(40, 108)
        Me.tbx31.Name = "tbx31"
        Me.tbx31.Size = New System.Drawing.Size(78, 26)
        Me.tbx31.TabIndex = 9
        '
        'tbx44
        '
        Me.tbx44.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx44.Location = New System.Drawing.Point(356, 150)
        Me.tbx44.Name = "tbx44"
        Me.tbx44.Size = New System.Drawing.Size(78, 26)
        Me.tbx44.TabIndex = 16
        '
        'tbx43
        '
        Me.tbx43.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx43.Location = New System.Drawing.Point(249, 150)
        Me.tbx43.Name = "tbx43"
        Me.tbx43.Size = New System.Drawing.Size(78, 26)
        Me.tbx43.TabIndex = 15
        '
        'tbx42
        '
        Me.tbx42.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx42.Location = New System.Drawing.Point(147, 150)
        Me.tbx42.Name = "tbx42"
        Me.tbx42.Size = New System.Drawing.Size(78, 26)
        Me.tbx42.TabIndex = 14
        '
        'tbx41
        '
        Me.tbx41.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbx41.Location = New System.Drawing.Point(40, 150)
        Me.tbx41.Name = "tbx41"
        Me.tbx41.Size = New System.Drawing.Size(78, 26)
        Me.tbx41.TabIndex = 13
        '
        'tbxResM4X4
        '
        Me.tbxResM4X4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbxResM4X4.Location = New System.Drawing.Point(116, 234)
        Me.tbxResM4X4.Name = "tbxResM4X4"
        Me.tbxResM4X4.Size = New System.Drawing.Size(190, 26)
        Me.tbxResM4X4.TabIndex = 17
        '
        'lblResultado
        '
        Me.lblResultado.AutoSize = True
        Me.lblResultado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultado.Location = New System.Drawing.Point(13, 237)
        Me.lblResultado.Name = "lblResultado"
        Me.lblResultado.Size = New System.Drawing.Size(82, 20)
        Me.lblResultado.TabIndex = 18
        Me.lblResultado.Text = "Resultado"
        '
        'form4x4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 323)
        Me.Controls.Add(Me.lblResultado)
        Me.Controls.Add(Me.tbxResM4X4)
        Me.Controls.Add(Me.tbx44)
        Me.Controls.Add(Me.tbx43)
        Me.Controls.Add(Me.tbx42)
        Me.Controls.Add(Me.tbx41)
        Me.Controls.Add(Me.tbx34)
        Me.Controls.Add(Me.tbx33)
        Me.Controls.Add(Me.tbx32)
        Me.Controls.Add(Me.tbx31)
        Me.Controls.Add(Me.tbx24)
        Me.Controls.Add(Me.tbx23)
        Me.Controls.Add(Me.tbx22)
        Me.Controls.Add(Me.tbx21)
        Me.Controls.Add(Me.tbx14)
        Me.Controls.Add(Me.tbx13)
        Me.Controls.Add(Me.tbx12)
        Me.Controls.Add(Me.tbx11)
        Me.Controls.Add(Me.btnCalcular4x4)
        Me.Name = "form4x4"
        Me.Text = "Determinante de 4x4"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCalcular4x4 As Button
    Friend WithEvents tbx11 As TextBox
    Friend WithEvents tbx12 As TextBox
    Friend WithEvents tbx13 As TextBox
    Friend WithEvents tbx14 As TextBox
    Friend WithEvents tbx24 As TextBox
    Friend WithEvents tbx23 As TextBox
    Friend WithEvents tbx22 As TextBox
    Friend WithEvents tbx21 As TextBox
    Friend WithEvents tbx34 As TextBox
    Friend WithEvents tbx33 As TextBox
    Friend WithEvents tbx32 As TextBox
    Friend WithEvents tbx31 As TextBox
    Friend WithEvents tbx44 As TextBox
    Friend WithEvents tbx43 As TextBox
    Friend WithEvents tbx42 As TextBox
    Friend WithEvents tbx41 As TextBox
    Friend WithEvents tbxResM4X4 As TextBox
    Friend WithEvents lblResultado As Label
End Class
