﻿Public Class form3X3
    Private Function matris3x3() As Decimal
        Dim dPrincipal As Decimal
        Dim dSecundaria As Decimal
        Dim dDeterminante As Decimal
        Dim m3x3(8) As Decimal
        Dim matrisTextBox() As TextBox = {tbx11, tbx12, tbx13, tbx21, tbx22, tbx23, tbx31, tbx32, tbx33}
        For i = 0 To 8
            m3x3(i) = matrisTextBox(i).Text
        Next i
        dPrincipal = (m3x3(0) * m3x3(4) * m3x3(8)) + (m3x3(1) * m3x3(5) * m3x3(6)) + (m3x3(3) * m3x3(7) * m3x3(2))
        dSecundaria = (m3x3(2) * m3x3(4) * m3x3(6)) + (m3x3(0) * m3x3(7) * m3x3(5)) + (m3x3(1) * m3x3(3) * m3x3(8))
        dDeterminante = dPrincipal - dSecundaria
        Return dDeterminante
    End Function
    Private Sub btnCalcular3x3_Click(sender As Object, e As EventArgs) Handles btnCalcular3x3.Click
        tbxResM3X3.Text = matris3x3()
    End Sub
End Class